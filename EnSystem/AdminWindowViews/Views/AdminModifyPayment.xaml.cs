﻿using EnSystem.Classes;
using System;
using System.Data;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;


namespace EnSystem.AdminWindowViews.Views
{
    /// <summary>
    /// Interaction logic for AdminModifyPayment.xaml
    /// </summary>
    public partial class AdminModifyPayment : UserControl
    {
        OleDbConnection conn = new OleDbConnection(@"Provider=Microsoft.Ace.OLEDB.12.0;Data Source= '" + dbconn.path + "'");
        static string status,yearlevel,labfee,tuitionfee,_miscfee,increase,course;
        private bool WasClicked = false;
        private void comboCourse()
        {
            string select = "SELECT tbl_Courses.T_Course FROM tbl_Courses; ";
            conn.Open();
            OleDbCommand cmd = new OleDbCommand(select, conn);
            OleDbDataReader DR = cmd.ExecuteReader();
            while (DR.Read())
            {
                CB_Course.Items.Add(DR[0]);
            }
            conn.Close();
        }
        public AdminModifyPayment()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            InitializeComponent();
            BindGrid();
            comboCourse();
            CB_Status.SelectedIndex = 0;
            CB_YearLevel.SelectedIndex = 0;
            CB_Course.SelectedIndex = 0;
        }
        private void CB_YearLevel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Check();
        }
       
        private void CB_Status_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Check();
        }

        private void BTN_Cancel_Click(object sender, RoutedEventArgs e)
        {
            ClearAll();
        }

        private void BTN_Update_Click(object sender, RoutedEventArgs e)
        {
            #region BTN UPDATE
            WasClicked = true;
            try
            {
                if (CB_Course.Text != "" && TB_Increase.Text != "" && TB_LabFee.Text != "" && TB_MiscFee.Text != "" && TB_TuitionFee.Text != "")
                {
                    if (CB_Status.SelectedIndex != 0 || CB_YearLevel.SelectedIndex != 0)
                    {
                        if (CB_Course.SelectedIndex > 0)
                        {
                            if (MessageBox.Show("You are updating course payment", "Updating payment", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                            {
                                using (OleDbCommand cmd = new OleDbCommand("UPDATE tbl_Tuition SET T_Status= @status,T_YearLevel= @yearlevel,T_MiscFee= @miscfee,T_LabFee= @labfee,T_TuitionFee = @tuitionfee,T_Increase= @increase,T_Course= @course WHERE T_Course ='" + course + "' AND T_YearLevel= '" + yearlevel + "' AND T_Status= '" + status + "' ", conn))
                                {

                                    cmd.CommandType = CommandType.Text;
                                    cmd.Parameters.AddWithValue("@status", CB_Status.Text);
                                    cmd.Parameters.AddWithValue("@yearlevel", CB_YearLevel.Text);
                                    cmd.Parameters.AddWithValue("@miscfee", Convert.ToDouble(TB_MiscFee.Text));
                                    cmd.Parameters.AddWithValue("@labfee", Convert.ToDouble(TB_LabFee.Text));
                                    cmd.Parameters.AddWithValue("@tuitionfee", Convert.ToDouble(TB_TuitionFee.Text));
                                    cmd.Parameters.AddWithValue("@increase", Convert.ToDouble(TB_Increase.Text));
                                    cmd.Parameters.AddWithValue("@course", CB_Course.Text);
                                    conn.Open();
                                    cmd.ExecuteNonQuery();
                                    conn.Close();
                                    MessageBox.Show("Successfully updated to database");
                                }
                            }
                            ClearAll();
                            BindGrid();
                            WasClicked = false;
                        }
                        else
                        {
                            MessageBox.Show("Select Course");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please select Year Level and Status");
                    }
                }
                else
                {
                    MessageBox.Show("Please answer the required field");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex);
            }
            finally
            {
                conn.Close();
            }
            #endregion BTN UPDATE
        }

        private void TB_Search_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            #region SEARCH
            WasClicked = true;
            try
            {
                DG_Payment.UnselectAllCells();
                DataTable searchTable = new DataTable();
                string searchUser = "SELECT tbl_Tuition.T_Course AS Course,tbl_Tuition.T_Status AS Student_Status, tbl_Tuition.T_YearLevel AS YearLevel, tbl_Tuition.T_MiscFee AS MiscFee, tbl_Tuition.T_LabFee AS LabFee, tbl_Tuition.T_TuitionFee AS TuitionFee, tbl_Tuition.T_Increase AS Increase " +
                    "FROM tbl_Tuition WHERE tbl_Tuition.T_Course LIKE '%" + TB_Search.Text + "%' OR tbl_Tuition.T_Status LIKE '%" + TB_Search.Text + "%' OR tbl_Tuition.T_YearLevel LIKE '%" + TB_Search.Text + "%' ";
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(searchUser, conn);
                dataAdapter.Fill(searchTable);
                DG_Payment.ItemsSource = searchTable.AsDataView();
                DG_Payment.Items.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR: " + ex);
            }
            finally
            {
                conn.Close();
                WasClicked = false;
            }
            #endregion
        }

        private void BTN_Delete_Click(object sender, RoutedEventArgs e)
        {
            #region BTN DELETE
            WasClicked = true;
            try
            {
                if (CB_Course.Text!="" && CB_Status.SelectedIndex>0 && CB_YearLevel.SelectedIndex>0)
                {
                    if (MessageBox.Show("You are deleting from item database", "Deleting from database", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        OleDbCommand cmd = new OleDbCommand("DELETE tbl_Tuition.* FROM tbl_Tuition WHERE (T_Course= '"+ course+ "' AND T_Status = '" +status + "' AND T_YearLevel = '" + yearlevel + "')", conn);
                        {
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();                           
                        }                       
                    }
                    MessageBox.Show("Successfully deleted item in database");
                    ClearAll();
                    BindGrid();
                    WasClicked = false;
                }
                else
                {
                    MessageBox.Show("Please select the item you want to delete");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex);
            }
            finally
            {
                conn.Close();
            }
            #endregion BTN DELETE
        }

        private void BindGrid()
        {
            #region BINDGRID
            try
            {
                OleDbCommand cmd = new OleDbCommand();
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT tbl_Tuition.T_Course AS Course,tbl_Tuition.T_Status AS Student_Status, tbl_Tuition.T_YearLevel AS YearLevel, tbl_Tuition.T_MiscFee AS MiscFee, tbl_Tuition.T_LabFee AS LabFee, tbl_Tuition.T_TuitionFee AS TuitionFee, tbl_Tuition.T_Increase AS Increase FROM tbl_Tuition";
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    DG_Payment.ItemsSource = dt.AsDataView();
                    conn.Close();
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error "+ex);
            }
            #endregion BINDGRID
        }

        private void BTN_Add_Click(object sender, RoutedEventArgs e)
        {
            #region BTN ADD
            WasClicked = true;
            try
            {
                if (CB_Course.Text != "" && TB_Increase.Text !="" && TB_LabFee.Text != "" && TB_MiscFee.Text !="" && TB_TuitionFee.Text !="")
                {
                    if (CB_Status.SelectedIndex != 0 || CB_YearLevel.SelectedIndex !=0)
                    {
                        if (CB_Course.SelectedIndex > 0)
                        {
                            if (MessageBox.Show("You are adding course payment", "Adding payment", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                            {
                                using (OleDbCommand cmd = new OleDbCommand("INSERT INTO tbl_Tuition(T_Status,T_YearLevel,T_MiscFee,T_LabFee,T_TuitionFee,T_Increase,T_Course)VALUES(?,?,?,?,?,?,?);", conn))
                                {

                                    cmd.CommandType = CommandType.Text;
                                    cmd.Parameters.AddWithValue("T_Status", CB_Status.Text);
                                    cmd.Parameters.AddWithValue("T_YearLevel", CB_YearLevel.Text);
                                    cmd.Parameters.AddWithValue("T_MiscFee", Convert.ToDouble(TB_MiscFee.Text));
                                    cmd.Parameters.AddWithValue("T_LabFee", Convert.ToDouble(TB_LabFee.Text));
                                    cmd.Parameters.AddWithValue("T_TuitionFee", Convert.ToDouble(TB_TuitionFee.Text));
                                    cmd.Parameters.AddWithValue("T_Increase", Convert.ToDouble(TB_Increase.Text));
                                    cmd.Parameters.AddWithValue("T_Course", CB_Course.Text);
                                    conn.Open();
                                    cmd.ExecuteNonQuery();
                                    conn.Close();
                                    MessageBox.Show("Successfully added to database");
                                }
                            }
                            BindGrid();
                            ClearAll();
                            WasClicked = false;
                        }
                        else
                        {
                            MessageBox.Show("Select course");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please select Year Level and Status");
                    }
                }
                else
                {
                    MessageBox.Show("Please answer the required field");
                }

            }
            catch(Exception ex)
            {
                MessageBox.Show("Error "+ex);
            }
            finally
            {
                conn.Close();
            }
            #endregion BTN ADD
        }

        private void DG_Payment_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            #region DG GET DATA
            if (WasClicked == false)
            {
                ClearAll();
                DataRowView dataRow = (DataRowView)DG_Payment.SelectedItems[0];
                status = Convert.ToString(dataRow.Row["Student_Status"]);
                yearlevel = Convert.ToString(dataRow.Row["YearLevel"]);
                _miscfee = Convert.ToString(dataRow.Row["MiscFee"]);
                labfee = Convert.ToString(dataRow.Row["LabFee"]);
                tuitionfee = Convert.ToString(dataRow.Row["TuitionFee"]);
                increase = Convert.ToString(dataRow.Row["Increase"]);
                course = Convert.ToString(dataRow.Row["Course"]);
                CB_Status.Text = status;
                CB_YearLevel.Text = yearlevel;
                TB_MiscFee.Text = _miscfee;
                TB_LabFee.Text = labfee;
                TB_TuitionFee.Text = tuitionfee;
                TB_Increase.Text = increase;
                CB_Course.Text = course;
            }
            #endregion
        }
        private void Check()
        {
            #region CHECK COMBO BOX
            if (CB_Status.SelectedIndex == 1 && CB_YearLevel.SelectedIndex > 1)
            {
                MessageBox.Show("You cannot select New Student if year level is not 1st Year ");
                CB_Status.SelectedIndex = 0;
                CB_YearLevel.SelectedIndex = 0;
            }
            else if (CB_Status.SelectedIndex == 3 && CB_YearLevel.SelectedIndex == 1)
            {
                MessageBox.Show("You cannot select Transfer student if year level is one ");
                CB_Status.SelectedIndex = 0;
                CB_YearLevel.SelectedIndex = 0;
            }
            else if (CB_YearLevel.SelectedIndex == 1 && CB_Status.SelectedIndex == 2)
            {
                MessageBox.Show("You cannot select Old student if year level is one");
                CB_Status.SelectedIndex = 0;
                CB_YearLevel.SelectedIndex = 0;
            }
            #endregion
        }
        private void ClearAll()
        {
            TB_Increase.Text = TB_LabFee.Text = TB_MiscFee.Text = TB_TuitionFee.Text = "";
            CB_Status.SelectedIndex= CB_Course.SelectedIndex = CB_YearLevel.SelectedIndex = 0;
        }
        #region REGEX
        private void TB_TuitionFee_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = Regex.IsMatch(e.Text, "[^0-9,.]+");
        }

        private void TB_MiscFee_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = Regex.IsMatch(e.Text, "[^0-9,.]+");
        }

        private void TB_LabFee_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = Regex.IsMatch(e.Text, "[^0-9,.]+");
        }

        private void TB_Increase_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = Regex.IsMatch(e.Text, "[^0-9,.]+");
        }
        #endregion
    }
}
