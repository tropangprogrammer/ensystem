﻿using System.Data.OleDb;
using EnSystem.Classes;
using System.Windows.Controls;
using System;
using System.Windows;
using System.Data;

namespace EnSystem.AdminWindowViews.Views
{
    /// <summary>
    /// Interaction logic for AdminCourses.xaml
    /// </summary>
    public partial class AdminCourses : UserControl
    {
        private bool WasClicked = false;
        OleDbConnection conn = new OleDbConnection(@"Provider=Microsoft.Ace.OLEDB.12.0;Data Source= '" + dbconn.path + "'");
        static string course, coursedescript;
        
        public AdminCourses()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory",Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            InitializeComponent();
            BindGrid();
        }
        private void BindGrid()
        {
            #region BINDGRID
            OleDbCommand cmd = new OleDbCommand();
            if (conn.State != ConnectionState.Open)
            {
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = "SELECT T_Course AS Course,C_CourseDescription AS CourseDecription FROM tbl_courses";
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                DG_COURSE.ItemsSource = dt.AsDataView();
                conn.Close();
            }
            #endregion
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            #region ADD
            WasClicked = true;

            try
            {
                if(TB_COURSE.Text!="" && TB_CourseDescription.Text!="")
                {
                    if (MessageBox.Show("You are adding a course", "Adding course", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        using (OleDbCommand cmd = new OleDbCommand("INSERT INTO tbl_Courses(T_Course,C_CourseDescription)VALUES(?,?);", conn))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("T_Course", TB_COURSE.Text);
                            cmd.Parameters.AddWithValue("C_CourseDescription", TB_CourseDescription.Text);
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                        MessageBox.Show("Successfully Added Course");
                        ClearAll();
                        BindGrid();
                        WasClicked = false;
                    }                  
                }
                else
                {
                    MessageBox.Show("Answer The Required Field", "Please try again", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }

            }
            catch(Exception Ex)
            {
                MessageBox.Show("Error:"+Ex);
            }
            finally
            {
                conn.Close();
            }
            #endregion
        }

        private void BTN_UPDATE_Click(object sender, RoutedEventArgs e)
        {
            #region UPDATE
            WasClicked = true;
            try
            {
                if (TB_COURSE.Text != "" && TB_CourseDescription.Text != "")
                {
                    if (MessageBox.Show("You are updating a course", "Adding course", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        using (OleDbCommand cmd = new OleDbCommand("UPDATE tbl_Courses SET T_Course = @Course,C_CourseDescription=@Course WHERE T_Course = '"+ course + "' AND C_CourseDescription = '" + coursedescript + "';", conn))
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.Parameters.AddWithValue("@Course", TB_COURSE.Text);
                            cmd.Parameters.AddWithValue("@CourseDescript", TB_CourseDescription.Text);
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                        MessageBox.Show("Successfully Updated Course");
                        ClearAll();
                        BindGrid();
                        WasClicked = false;
                    }
                }
                else
                {
                    MessageBox.Show("Please select course to be updated", "Please try again", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show("Error:" + Ex);
            }
            finally
            {
                conn.Close();
            }
            #endregion

        }

        private void DG_COURSE_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (WasClicked == false)
            {
                ClearAll();
                DataRowView dataRow = (DataRowView)DG_COURSE.SelectedItems[0];
                course = Convert.ToString(dataRow.Row["Course"]);
                coursedescript = Convert.ToString(dataRow.Row["CourseDecription"]);
                TB_COURSE.Text = course;
                TB_CourseDescription.Text = coursedescript;
            }
        }

        private void BTN_DELETE_Click(object sender, RoutedEventArgs e)
        {
            #region DELETE
            WasClicked = true;
            try
            {
                if (TB_COURSE.Text != "" && TB_CourseDescription.Text != "")
                {
                    if (MessageBox.Show("You are deleting a course", "Delete course", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        using (OleDbCommand cmd = new OleDbCommand("DELETE tbl_Courses.* FROM tbl_Courses WHERE T_Course = '" + course +"'", conn))
                        {
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                        MessageBox.Show("Successfully Deleted Course");
                        BindGrid();
                        ClearAll();
                        WasClicked = false;
                    }
                }
                else
                {
                    MessageBox.Show("Please select course to be deleted", "Please try again", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }

            }
            catch (Exception Ex)
            {
                MessageBox.Show("Error:" + Ex);
            }
            finally
            {
                conn.Close();
            }
            #endregion
        }

        private void BTN_CLEAR_Click(object sender, RoutedEventArgs e)
        {
            ClearAll();
        }

        private void ClearAll()
        {
            TB_COURSE.Text = "";
            TB_CourseDescription.Text = "";
        }
    }
}
