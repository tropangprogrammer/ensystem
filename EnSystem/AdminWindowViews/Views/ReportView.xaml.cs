﻿using System.Windows;
using System.Windows.Controls;
using EnSystem.AdminReports;

namespace EnSystem.AdminWindowViews.Views
{
    /// <summary>
    /// Interaction logic for ReportView.xaml
    /// </summary>
    public partial class ReportView : UserControl
    {
        public static bool BtnUserReportClicked;
        public static bool BtnEnrollReportClicked;
        public ReportView()
        {
            InitializeComponent();
        }

        private void BTN_USER_Click(object sender, RoutedEventArgs e)
        {
            BtnUserReportClicked = true;
            ReportWindow window = new ReportWindow();
            window.Show();
        }

        private void BTN_ENROLLEE_Click(object sender, RoutedEventArgs e)
        {
            BtnEnrollReportClicked = true;
            ReportWindow window = new ReportWindow();
            window.Show();
        }
    }
}
