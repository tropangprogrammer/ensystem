﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Data.OleDb;
using System.Data;
using EnSystem.Classes;
using System.Text.RegularExpressions;

namespace EnSystem.AdminWindowViews.Views
{
    /// <summary>
    /// Interaction logic for AdminManageUsers.xaml
    /// </summary>
    public partial class AdminManageUsers : UserControl
    {
        OleDbConnection conn = new OleDbConnection(@"Provider=Microsoft.Ace.OLEDB.12.0;Data Source= '"+dbconn.path+"'");
        string AddToInfo = "INSERT INTO tbl_Info(i_fname,i_Lname,i_IdNumber,T_Course,i_contactNum)VALUES(?,?,?,?,?);";
        string AddToLogin = "INSERT INTO tbl_Login(e_usern,e_passw,i_IdNumber,e_is_admin)VALUES(?,?,?,?);";
        static string firstname,lastname,username,userid,course,contact,password,Temp,isadmin;
        private bool WasClicked = false;

        private void comboCourse()
        {
            #region Select Course from Database
            string select = "SELECT tbl_Courses.T_Course FROM tbl_Courses; ";
            conn.Open();
            OleDbCommand cmd = new OleDbCommand(select, conn);
            OleDbDataReader DR = cmd.ExecuteReader();
            while (DR.Read())
            {
                CB_Course.Items.Add(DR[0]);
            }
            conn.Close();
            #endregion
        }

        private void BindGrid()
        {
            #region BINDGRID
            OleDbCommand cmd = new OleDbCommand();
            if (conn.State != ConnectionState.Open)
            {           
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = "SELECT tbl_Info.i_fname AS Firstname, tbl_Info.i_Lname AS Lastname, tbl_Login.e_usern AS Username, tbl_Info.i_IdNumber AS UserId, tbl_Info.T_Course AS Course, tbl_Info.i_contactNum AS Contact, tbl_Login.e_is_admin AS AdminAccess, tbl_Login.e_passw AS Passphrase FROM tbl_Info INNER JOIN tbl_Login ON tbl_Info.i_IdNumber = tbl_Login.i_IdNumber"; 
                OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                UserGrid.ItemsSource = dt.AsDataView();
                conn.Close();
            }
            #endregion
        }

        private void BTN_Cancel_Click(object sender, RoutedEventArgs e)
        {
            ClearAll();
        }

        private void TB_SearchUser_KeyUp(object sender, KeyEventArgs e)
        {
            #region SEARCH
            WasClicked = true;
            try
            {
                UserGrid.UnselectAllCells();
                DataTable searchTable = new DataTable();
                string searchUser = "Select tbl_Info.i_fname AS Firstname, tbl_Info.i_Lname AS Lastname, tbl_Login.e_usern AS Username, tbl_Info.i_IdNumber AS UserId, tbl_Info.T_Course AS Course, tbl_Info.i_contactNum AS Contact, tbl_Login.e_is_admin AS AdminAccess, tbl_Login.e_passw AS Passphrase " +
                    "FROM tbl_Info INNER JOIN tbl_Login ON tbl_Info.i_IdNumber = tbl_Login.i_IdNumber WHERE tbl_login.e_usern LIKE '%" + TB_SearchUser.Text + "%' OR tbl_Info.i_fname LIKE '%" + TB_SearchUser.Text + "%' OR  tbl_Info.i_Lname LIKE '%" + TB_SearchUser.Text + "%' OR tbl_Info.T_Course LIKE '%" + TB_SearchUser.Text + "%' OR tbl_Info.i_IdNumber LIKE '%" + TB_SearchUser.Text + "%' ";
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(searchUser, conn);
                dataAdapter.Fill(searchTable); 
                UserGrid.ItemsSource = searchTable.AsDataView();
                UserGrid.Items.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR: " + ex);
            }
            finally
            {
                conn.Close();
                WasClicked = false;
            }
            #endregion
        }

        public AdminManageUsers()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            InitializeComponent();
            BindGrid();
            comboCourse();
            CB_Course.SelectedIndex = 0;
            CB_Isadmin.SelectedIndex = 0;
        }

        private void UserGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            #region USERGRID
            if (WasClicked==false)
            {
                DataRowView dataRow = (DataRowView)UserGrid.SelectedItems[0];
                firstname = Convert.ToString(dataRow.Row["Firstname"]);
                lastname = Convert.ToString(dataRow.Row["Lastname"]);
                username = Convert.ToString(dataRow.Row["Username"]);
                userid = Convert.ToString(dataRow.Row["UserId"]);
                course = Convert.ToString(dataRow.Row["Course"]);
                contact = Convert.ToString(dataRow.Row["Contact"]);
                password = Convert.ToString(dataRow.Row["Passphrase"]);
                isadmin = Convert.ToString(dataRow.Row["AdminAccess"]);
                TB_Firstname.Text = firstname;
                TB_Lastname.Text = lastname;
                TB_Username.Text = username;
                TB_Idnumber.Text = userid;
                CB_Course.Text = course;
                TB_Contact.Text = contact;
                TB_Password.Text = password;
                CB_Isadmin.Text = isadmin;
            }
            #endregion
        }


        private void BTN_Add_Click(object sender, RoutedEventArgs e)
        {
            #region ADD BUTTON
            WasClicked = true;
            try
            {
                //Field must not be empty
                if (TB_Firstname.Text != "" && TB_Lastname.Text != "" && TB_Username.Text != ""
                    && TB_Idnumber.Text != "" && TB_Contact.Text != "" && TB_Password.Text != "")
                {
                    if (CB_Course.SelectedIndex > 0)
                    {
                        if (CB_Isadmin.Text != "" && CB_Isadmin.SelectedIndex > 0)
                        {
                            if (MessageBox.Show("You are adding " + TB_Firstname.Text + " " + TB_Lastname.Text + " into Users", "Adding user", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                            {
                                using (OleDbCommand cmd = new OleDbCommand(AddToInfo, conn))
                                {

                                    cmd.CommandType = CommandType.Text;
                                    cmd.Parameters.AddWithValue("i_fname", TB_Firstname.Text);
                                    cmd.Parameters.AddWithValue("i_Lname", TB_Lastname.Text);
                                    cmd.Parameters.AddWithValue("i_IdNumber", TB_Idnumber.Text);
                                    cmd.Parameters.AddWithValue("T_Course", CB_Course.Text);
                                    cmd.Parameters.AddWithValue("i_contactNum", TB_Contact.Text);
                                    conn.Open();
                                    cmd.ExecuteNonQuery();
                                    conn.Close();
                                }
                                using (OleDbCommand cmd = new OleDbCommand(AddToLogin, conn))
                                {
                                    cmd.CommandType = CommandType.Text;
                                    cmd.Parameters.AddWithValue("e_usern", TB_Username.Text);
                                    cmd.Parameters.AddWithValue("e_passw", TB_Password.Text);
                                    cmd.Parameters.AddWithValue("i_IdNumber", TB_Idnumber.Text);
                                    cmd.Parameters.AddWithValue("e_is_admin", CB_Isadmin.Text);
                                    conn.Open();
                                    cmd.ExecuteNonQuery();
                                    conn.Close();
                                }
                                MessageBox.Show("Successfully Added User");
                                BindGrid();
                                ClearAll();
                                WasClicked = false;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Select user admin access", "Please try again", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Select user course", "Please try again", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                    
                }
                else
                {
                    MessageBox.Show("Answer The Required Field", "Please try again", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error "+ex);
            }
            finally
            {
                conn.Close();              
            }
            #endregion ADD BUTTON
        }

        private void BTN_Update_Click(object sender, RoutedEventArgs e)
        {
            #region UPDATE BUTTON
            WasClicked = true;
            Temp = TB_Idnumber.Text;
            try
            {
                //Field must not be empty
                if (TB_Firstname.Text != "" && TB_Lastname.Text != "" && TB_Username.Text != ""
                    && TB_Idnumber.Text != "" && TB_Contact.Text != "" && TB_Password.Text != "")
                {
                    if (CB_Isadmin.Text != "" && CB_Isadmin.SelectedIndex>0)
                    {
                        if (CB_Course.SelectedIndex > 0)
                        {
                            if (MessageBox.Show("You are Updating " + firstname + " " + lastname, "Adding user", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                            {
                                string UPDATEInfo = "UPDATE tbl_Info SET i_fname=@Fname,i_Lname=@Lname,i_IdNumber=@ID,T_Course=@Course,i_contactNum=@Contact WHERE i_IdNumber = '" + userid + "'";
                                string UPDATELogin = "UPDATE tbl_Login SET e_usern=@User,e_passw=@PASS,i_IdNumber=@ID,e_is_admin=@IsAdmin WHERE i_IdNumber = '" + userid + "'";
                                using (OleDbCommand cmd = new OleDbCommand(UPDATEInfo, conn))
                                {
                                    cmd.CommandType = CommandType.Text;
                                    cmd.Parameters.AddWithValue("@Fname", TB_Firstname.Text);
                                    cmd.Parameters.AddWithValue("@Lname", TB_Lastname.Text);
                                    cmd.Parameters.AddWithValue("@ID", TB_Idnumber.Text);
                                    cmd.Parameters.AddWithValue("@Course", CB_Course.Text);
                                    cmd.Parameters.AddWithValue("@Contact", TB_Contact.Text);
                                    conn.Open();
                                    cmd.ExecuteNonQuery();
                                    conn.Close();

                                }
                                using (OleDbCommand cmd = new OleDbCommand(UPDATELogin, conn))
                                {
                                    cmd.CommandType = CommandType.Text;
                                    cmd.Parameters.AddWithValue("@User", TB_Username.Text);
                                    cmd.Parameters.AddWithValue("@PASS", TB_Password.Text);
                                    cmd.Parameters.AddWithValue("@ID", TB_Idnumber.Text);
                                    cmd.Parameters.AddWithValue("@IsAdmin", CB_Isadmin.Text);
                                    conn.Open();
                                    cmd.ExecuteNonQuery();
                                    conn.Close();

                                }
                                MessageBox.Show("Successfully Updated User Info");
                                BindGrid();
                                ClearAll();
                                WasClicked = false;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Select user course", "Please try again", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Select user admin access", "Please try again", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                }
                else
                {
                    MessageBox.Show("Answer The Required Field", "Please try again", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex);
            }
            finally
            {               
                conn.Close();
            }
            
            #endregion UPDATE BUTTON
        }

        private void BTN_Delete_Click(object sender, RoutedEventArgs e)
        {          
            #region DELETE BUTTON
            WasClicked = true;
            string TEMP_ID = TB_Idnumber.Text;
            try
            {
                if (TB_Idnumber.Text != "" && TB_Username.Text != "")
                {                    
                    if (MessageBox.Show("You are deleting " + username, "Delete", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {                        
                        OleDbCommand oleDbCommand = new OleDbCommand("DELETE tbl_Info.*, tbl_Login.*, tbl_Login.i_IdNumber FROM tbl_Info INNER JOIN tbl_Login ON tbl_Info.i_IdNumber = tbl_Login.i_IdNumber WHERE(((tbl_Login.i_IdNumber)= '"+TB_Idnumber.Text+"'))", conn);
                        conn.Open();
                        oleDbCommand.ExecuteNonQuery();
                        MessageBox.Show("Successfully Deleted User Info");
                        conn.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Please select the user you want to delete");
                }
                BindGrid();
                ClearAll();
                WasClicked = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex);
            }
            finally
            {                
                conn.Close();
            }           
            #endregion DELETE BUTTON
        }
        private void ClearAll()
        {
            TB_Contact.Text=TB_Firstname.Text = TB_Idnumber.Text = TB_Lastname.Text = TB_Password.Text = TB_Username.Text = TB_Idnumber.Text ="";
            CB_Isadmin.SelectedIndex = CB_Course.SelectedIndex = 0;
        }
        #region REGEX
        private void TB_Idnumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = Regex.IsMatch(e.Text, "[^0-9,-]+");
        }
        private void TB_Contact_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = Regex.IsMatch(e.Text, "[^0-9]+");
        }
        #endregion
    }
}
