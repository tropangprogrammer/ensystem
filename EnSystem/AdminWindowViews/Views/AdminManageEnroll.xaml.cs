﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Data.OleDb;
using EnSystem.Classes;
using System.Data;
using System.Text.RegularExpressions;

namespace EnSystem.AdminWindowViews.Views
{
    /// <summary>
    /// Interaction logic for AdminManageEnroll.xaml
    /// </summary>
    public partial class AdminManageEnroll : UserControl
    {
        OleDbConnection conn = new OleDbConnection(@"Provider=Microsoft.Ace.OLEDB.12.0;Data Source= '" + dbconn.path + "'");
        private static string course, total, dateEnrolled, dateFinished, ispayed, yearlevel, status, idnumber,Completedate;
        private bool WasClicked = false;
        public AdminManageEnroll()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory",Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            InitializeComponent();
            BindGrid();
            comboCourse();
        }
        private void comboCourse()
        {
            #region select course
            string select = "SELECT tbl_Courses.T_Course FROM tbl_Courses; ";
            conn.Open();
            OleDbCommand cmd = new OleDbCommand(select, conn);
            OleDbDataReader DR = cmd.ExecuteReader();
            while (DR.Read())
            {
                CB_Course.Items.Add(DR[0]);
            }
            conn.Close();
            #endregion
        }
        private void BindGrid()
        {
            #region BINDGRID
            try
            {
                OleDbCommand cmd = new OleDbCommand();
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                    cmd.Connection = conn;
                    cmd.CommandText = "SELECT tbl_Enroll.i_IdNumber AS IDNumber,tbl_Enroll.T_Course AS Course, tbl_Enroll.N_TotalFee AS TotalFee, tbl_Enroll.N_DateEnrolled AS DateEnrolled, tbl_Enroll.N_DateFinished AS DateFinished, tbl_Enroll.N_IsPayed AS IsPayed, tbl_Enroll.N_YearLevel AS YearLevel, tbl_Enroll.N_Status AS Status FROM tbl_Enroll";
                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    DG_Course.ItemsSource = dt.AsDataView();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex);
            }
            #endregion
        }

        private void BTN_ADD_Click(object sender, RoutedEventArgs e)
        {
            #region ADD
            WasClicked = true;
            if (TB_IdNumber.Text !="" && TB_Total.Text !="")
            {
                if (CB_Course.SelectedIndex>0)
                {
                    if (CB_Status.SelectedIndex>0 && CB_YearLevel.SelectedIndex>0)
                    {
                        if (CB_IsPayed.SelectedIndex>0)
                        {
                            if (DateEnroll.SelectedDate != null)
                            {
                                DateTime? Edate = DateEnroll.SelectedDate;
                                DateTime? Cdate = DateComplete.SelectedDate;
                                try
                                {
                                    if (Cdate == null || DateComplete.Text == "No Date")
                                    {
                                        Completedate = "No Date";
                                    }
                                    else
                                    {
                                        Completedate = Cdate.Value.ToLongDateString();
                                    }
                                    if (MessageBox.Show("You are adding to enrollments", "Adding to enrollment", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                                    {
                                        using (OleDbCommand cmd = new OleDbCommand("INSERT INTO tbl_Enroll(N_Status,N_YearLevel,i_IdNumber,N_TotalFee,N_DateEnrolled,N_DateFinished,N_IsPayed,T_Course)VALUES(?,?,?,?,?,?,?,?);", conn))
                                        {
                                            cmd.CommandType = CommandType.Text;
                                            cmd.Parameters.AddWithValue("N_Status", CB_Status.Text);
                                            cmd.Parameters.AddWithValue("N_YearLevel", CB_YearLevel.Text);
                                            cmd.Parameters.AddWithValue("i_IdNumber", TB_IdNumber.Text);
                                            cmd.Parameters.AddWithValue("N_TotalFee", Convert.ToDouble(TB_Total.Text));
                                            cmd.Parameters.AddWithValue("N_DataEnrolled", Edate.Value.ToLongDateString());
                                            cmd.Parameters.AddWithValue("N_DateFinished", Completedate);
                                            cmd.Parameters.AddWithValue("N_IsPayed", CB_IsPayed.Text);
                                            cmd.Parameters.AddWithValue("T_Course", CB_Course.Text);                                           
                                            conn.Open();
                                            cmd.ExecuteNonQuery();
                                            conn.Close();
                                            MessageBox.Show("Successfully added to database");
                                        }
                                    }
                                    BindGrid();
                                    clear();
                                    WasClicked = false;
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Error: " + ex);
                                }
                                finally
                                {
                                    conn.Close();
                                }
                            }
                            else
                            {
                                MessageBox.Show("Select Enrollment Date");
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Select Status and Year Level");
                    }

                }
                else
                {
                    MessageBox.Show("Select Course");
                }
                
            }
            else
            {
                MessageBox.Show("Please answer the required field");
            }
            #endregion
        }

        private void BTN_UPDATE_Click(object sender, RoutedEventArgs e)
        {
            #region UPDATE
            WasClicked = true;
            if (TB_IdNumber.Text != "" && TB_Total.Text != "")
            {
                if (CB_Course.SelectedIndex > 0)
                {
                    if (CB_Status.SelectedIndex > 0 && CB_YearLevel.SelectedIndex > 0)
                    {
                        if (CB_IsPayed.SelectedIndex > 0)
                        {
                            if (DateEnroll.SelectedDate != null)
                            {
                                DateTime? Edate = DateEnroll.SelectedDate;
                                DateTime? Cdate = DateComplete.SelectedDate;
                                try
                                {
                                    if (Cdate == null || DateComplete.Text == "No Date")
                                    {
                                        Completedate = "No Date";
                                    }
                                    else
                                    {
                                        Completedate = Cdate.Value.ToLongDateString();
                                    }
                                    if (MessageBox.Show("You are updating enrollment", "Updating enrollment", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                                    {
                                        using (OleDbCommand cmd = new OleDbCommand("UPDATE tbl_Enroll SET N_Status= @status ,N_YearLevel= @yearlevel ,i_IdNumber= @idnumber ,N_TotalFee= @totalfee ,N_DateEnrolled= @dateE ,N_DateFinished= @dateC ,N_IsPayed= @ispayed ,T_Course= @course WHERE i_IdNumber='"+idnumber+ "' AND N_Status ='" + status + "' AND N_YearLevel='"+yearlevel+"'", conn))
                                        {
                                            cmd.CommandType = CommandType.Text;
                                            cmd.Parameters.AddWithValue("@status", CB_Status.Text);
                                            cmd.Parameters.AddWithValue("@yearlevel", CB_YearLevel.Text);
                                            cmd.Parameters.AddWithValue("@idnumber", TB_IdNumber.Text);
                                            cmd.Parameters.AddWithValue("@totalfee", Convert.ToDouble(TB_Total.Text));
                                            cmd.Parameters.AddWithValue("@dateE", Edate.Value.ToLongDateString());
                                            cmd.Parameters.AddWithValue("@dateC", Completedate);
                                            cmd.Parameters.AddWithValue("@ispayed", CB_IsPayed.Text);
                                            cmd.Parameters.AddWithValue("@course", CB_Course.Text);
                                            conn.Open();
                                            cmd.ExecuteNonQuery();
                                            conn.Close();
                                            MessageBox.Show("Successfully updated in database");
                                        }
                                    }
                                    BindGrid();
                                    clear();
                                    WasClicked = false;
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Error: " + ex);
                                }
                                finally
                                {
                                    conn.Close();
                                }
                            }
                            else
                            {
                                MessageBox.Show("Select Enrollment Date");
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Select Status and Year Level");
                    }

                }
                else
                {
                    MessageBox.Show("Select Course");
                }

            }
            else
            {
                MessageBox.Show("Please answer the required field");
            }
            #endregion
        }

        private void TB_Search_KeyUp(object sender, KeyEventArgs e)
        {
            #region SEARCH
            WasClicked = true;
            try
            {
                DG_Course.UnselectAllCells();
                DataTable searchTable = new DataTable();
                string searchUser = "SELECT tbl_Enroll.i_IdNumber AS IDNumber,tbl_Enroll.T_Course AS Course, tbl_Enroll.N_TotalFee AS TotalFee, tbl_Enroll.N_DateEnrolled AS DateEnrolled, tbl_Enroll.N_DateFinished AS DateFinished, tbl_Enroll.N_IsPayed AS IsPayed, tbl_Enroll.N_YearLevel AS YearLevel, tbl_Enroll.N_Status AS Status " +
                    "FROM tbl_Enroll WHERE tbl_Enroll.i_IdNumber LIKE '%" + TB_Search.Text + "%' OR tbl_Enroll.T_Course LIKE '%" + TB_Search.Text + "%' OR tbl_Enroll.N_IsPayed LIKE '%" + TB_Search.Text + "%' ";
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(searchUser, conn);
                dataAdapter.Fill(searchTable);
                DG_Course.ItemsSource = searchTable.AsDataView();
                DG_Course.Items.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR: " + ex);
            }
            finally
            {
                conn.Close();
                WasClicked = false;
            }
            #endregion
        }

        private void Check()
        {
            #region CHECK COMBO BOX
            if (CB_Status.SelectedIndex == 1 && CB_YearLevel.SelectedIndex > 1)
            {
                MessageBox.Show("You cannot select New Student if year level is not 1st Year ");
                CB_Status.SelectedIndex = 0;
                CB_YearLevel.SelectedIndex = 0;
            }
            else if (CB_Status.SelectedIndex == 3 && CB_YearLevel.SelectedIndex == 1)
            {
                MessageBox.Show("You cannot select Transfer student if year level is one ");
                CB_Status.SelectedIndex = 0;
                CB_YearLevel.SelectedIndex = 0;
            }
            else if (CB_YearLevel.SelectedIndex == 1 && CB_Status.SelectedIndex == 2)
            {
                MessageBox.Show("You cannot select Old student if year level is one");
                CB_Status.SelectedIndex = 0;
                CB_YearLevel.SelectedIndex = 0;
            }
            #endregion
        }
        private void clear()
        {
            CB_Course.SelectedIndex= CB_IsPayed.SelectedIndex = CB_Status.SelectedIndex = CB_YearLevel.SelectedIndex = 0;
            TB_IdNumber.Text = TB_Total.Text = DateComplete.Text = DateEnroll.Text = "";
        }

        private void BTN_Cancel_Click(object sender, RoutedEventArgs e)
        {
            clear();
        }

        private void BTN_Delete_Click(object sender, RoutedEventArgs e)
        {
            #region BTN DELETE
            WasClicked = true;
            try
            {
                if (CB_Course.Text != "" && CB_Status.SelectedIndex > 0 && CB_YearLevel.SelectedIndex > 0)
                {
                    if (MessageBox.Show("You are deleting from item database", "Deleting from database", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        OleDbCommand cmd = new OleDbCommand("DELETE tbl_Enroll.* FROM tbl_Enroll WHERE (T_Course= '" + course + "' AND N_Status = '" + status + "' AND N_YearLevel = '" + yearlevel + "' AND i_IdNumber = '" + idnumber+ "')", conn);
                        {
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                        }
                    }
                    MessageBox.Show("Successfully deleted item in database");
                    clear();
                    BindGrid();
                    WasClicked = false;
                }
                else
                {
                    MessageBox.Show("Please select the item you want to delete");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex);
            }
            finally
            {
                conn.Close();
            }
            #endregion BTN DELETE
        }

        private void CB_YearLevel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Check();
        }

        private void CB_Status_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Check();
        }

        private void DG_Course_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            #region DG GET DATA
            if (WasClicked == false)
            {               
                clear();
                DataRowView dataRow = (DataRowView)DG_Course.SelectedItems[0];
                status = Convert.ToString(dataRow.Row["Status"]);
                yearlevel = Convert.ToString(dataRow.Row["YearLevel"]);
                total = Convert.ToString(dataRow.Row["TotalFee"]);
                dateEnrolled = Convert.ToString(dataRow.Row["DateEnrolled"]);
                dateFinished = Convert.ToString(dataRow.Row["DateFinished"]);
                ispayed = Convert.ToString(dataRow.Row["IsPayed"]);
                course = Convert.ToString(dataRow.Row["Course"]);
                idnumber = Convert.ToString(dataRow.Row["IDNumber"]);
                CB_Status.Text = status;
                CB_YearLevel.Text = yearlevel;
                TB_IdNumber.Text = idnumber;
                TB_Total.Text = total;
                CB_IsPayed.Text = ispayed;
                DateComplete.Text = dateFinished;
                DateEnroll.Text = dateEnrolled;
                CB_Course.Text = course;
            }
            #endregion
        }
        #region REGEX
        private void TB_IdNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = Regex.IsMatch(e.Text, "[^0-9,-]+");
        }

        private void TB_Total_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var textBox = sender as TextBox;
            e.Handled = Regex.IsMatch(e.Text, "[^0-9]+");
        }
        #endregion
    }
}
