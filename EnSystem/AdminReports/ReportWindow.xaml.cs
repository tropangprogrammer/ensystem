﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using EnSystem.AdminWindowViews.Views;

namespace EnSystem.AdminReports
{
    /// <summary>
    /// Interaction logic for ReportWindow.xaml
    /// </summary>
    public partial class ReportWindow : Window
    {
        private bool _isReportViewerLoaded;
        public ReportWindow()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory",Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            InitializeComponent();
            _reportViewer.Load += ReportViewer_Load;
        }
        private void ReportViewer_Load(object sender, EventArgs e)
        {
            if (!_isReportViewerLoaded)
            {
                

                
                if (ReportView.BtnUserReportClicked==true)
                {
                    ReportDataSource reportDS= new ReportDataSource();
                    EnrollSchoolDbDataSet dataset = new EnrollSchoolDbDataSet();

                    dataset.BeginInit();

                    reportDS.Name = "UserDataSet"; //Name of the report dataset in our .RDLC file
                    reportDS.Value = dataset.InfoReport;
                    this._reportViewer.LocalReport.DataSources.Add(reportDS);
                    this._reportViewer.LocalReport.ReportEmbeddedResource = "EnSystem.UserReport.rdlc";

                    dataset.EndInit();

                    //fill data into adventureWorksDataSet
                    EnrollSchoolDbDataSetTableAdapters.InfoReportTableAdapter adapter = new EnrollSchoolDbDataSetTableAdapters.InfoReportTableAdapter();
                    adapter.ClearBeforeFill = true;
                    adapter.Fill(dataset.InfoReport);

                    _reportViewer.RefreshReport();
                }
                if(ReportView.BtnEnrollReportClicked == true)
                {
                    ReportDataSource reportDS = new ReportDataSource();
                    EnrollSchoolDbDataSet1 dataset = new EnrollSchoolDbDataSet1();

                    dataset.BeginInit();

                    reportDS.Name = "EnrolleeDataSet"; //Name of the report dataset in our .RDLC file
                    reportDS.Value = dataset.EnrollReport;
                    this._reportViewer.LocalReport.DataSources.Add(reportDS);
                    this._reportViewer.LocalReport.ReportEmbeddedResource = "EnSystem.EnrolleeReport.rdlc";

                    dataset.EndInit();

                    //fill data into adventureWorksDataSet
                    EnrollSchoolDbDataSet1TableAdapters.EnrollReportTableAdapter adapter = new EnrollSchoolDbDataSet1TableAdapters.EnrollReportTableAdapter();
                    adapter.ClearBeforeFill = true;
                    adapter.Fill(dataset.EnrollReport);

                    _reportViewer.RefreshReport();
                }
                


                _isReportViewerLoaded = true;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ReportView.BtnEnrollReportClicked=false;
            ReportView.BtnUserReportClicked=false;
        }
    }

}
