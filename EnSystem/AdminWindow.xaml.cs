﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using EnSystem.AdminWindowViews.Views;

namespace EnSystem
{
    /// <summary>
    /// Interaction logic for AdminWindow.xaml
    /// </summary>
    public partial class AdminWindow : Window
    {
        public AdminWindow()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory",Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            InitializeComponent();
            DataContext = new AdminManageUsers();
            btn_User.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#007ACC"));
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Admin_Bar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void AdminManageUsers_Click(object sender, RoutedEventArgs e)
        {
            DataContext = new AdminManageUsers();
            btn_User.Foreground =(SolidColorBrush)(new BrushConverter().ConvertFrom("#007ACC"));
            btn_Courses.Foreground = btn_enrolled.Foreground = btn_matriculation.Foreground = btn_Report.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("LightGray"));
        }

        private void AdminManageEnroll_Click(object sender, RoutedEventArgs e)
        {
            DataContext = new AdminManageEnroll();
            btn_enrolled.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#007ACC"));
            btn_Courses.Foreground  = btn_User.Foreground = btn_matriculation.Foreground = btn_Report.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("LightGray"));
        }

        private void AdminModifyPayment_Click(object sender, RoutedEventArgs e)
        {
            DataContext = new AdminModifyPayment();
            btn_matriculation.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#007ACC"));
            btn_User.Foreground = btn_Courses.Foreground = btn_enrolled.Foreground = btn_Report.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("LightGray"));            
        }

        private void AdminCourses_Click(object sender, RoutedEventArgs e)
        {
            DataContext = new AdminCourses();
            btn_Courses.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#007ACC"));
            btn_User.Foreground = btn_enrolled.Foreground = btn_matriculation.Foreground = btn_Report.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("LightGray"));
        }
        private void ReportView_Click(object sender, RoutedEventArgs e)
        {
            DataContext = new ReportView();
            btn_Report.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#007ACC"));
            btn_User.Foreground = btn_enrolled.Foreground = btn_matriculation.Foreground = btn_Courses.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("LightGray"));
        }

        private void BTN_Logout_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Logout?", "Logging Out", MessageBoxButton.OKCancel, MessageBoxImage.Question) == MessageBoxResult.OK)
            {
                loginWindow gotologin = new loginWindow();
                this.Hide();
                gotologin.Show();
                this.Close();
            }
        }

    }
}
