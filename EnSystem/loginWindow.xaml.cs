﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Data.OleDb;
using System.Data;
using System.Reflection;
using EnSystem.Classes;


namespace EnSystem
{
    /// <summary>
    /// Interaction logic for loginWindow.xaml
    /// </summary>
    public partial class loginWindow : Window
    {
        public OleDbConnection conn = new OleDbConnection();
        public int count;
        MessageBoxResult LoginResult = new MessageBoxResult();
        public static string currentUser,fName,lName,userID;
        string IsAdmin;


        public loginWindow()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory",Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            InitializeComponent();
            conn.ConnectionString = @"Provider=Microsoft.Ace.OLEDB.12.0;Data Source= '"+dbconn.path+"' ";

        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            //_typedInto = !String.IsNullOrEmpty(Txtbox_username.Text);
        }

        private void Tab_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void btn_login_Click(object sender, RoutedEventArgs e)
        {
            #region User Login
            if (Txtbox_username.Text != "" && password.Password != "")
            {
                //@"SELECT * FROM tbl_Login WHERE e_usern = '" + Txtbox_username.Text + "' AND e_passw = '" + password.Password + "';";
                try
                {
                    conn.Open();
                    string selectUser = "SELECT tbl_Info.*, tbl_Login.* FROM tbl_Info INNER JOIN tbl_Login ON tbl_Info.i_IdNumber = tbl_Login.i_IdNumber WHERE e_usern = '" + Txtbox_username.Text + "' AND e_passw = '" + password.Password + "';";
                    OleDbCommand cmd = new OleDbCommand (selectUser);
                    cmd.Connection = conn;
                    OleDbDataReader read = cmd.ExecuteReader();
                    while (read.Read())
                    {
                        count = count + 1;
                        IsAdmin = read["e_is_admin"].ToString();
                        currentUser = read["e_usern"].ToString();
                        fName = read["i_fname"].ToString();
                        lName = read["i_Lname"].ToString();
                        userID = read["tbl_Login.i_IdNumber"].ToString();
                    }
                    if (read.HasRows)
                    {
                        currentUser = Txtbox_username.Text;
                        if (IsAdmin == "Yes")
                            {
                                LoginResult = MessageBox.Show("Logged in as Admin", "access granted", MessageBoxButton.OK);
                                if (LoginResult == MessageBoxResult.OK)
                                {
                                    this.Hide();
                                    AdminWindow aw = new AdminWindow();
                                    aw.Show();
                                    this.Close();
                                }
                            }
                            else
                            {
                                LoginResult = MessageBox.Show("Password and Username correct", "access granted", MessageBoxButton.OK);
                                if (LoginResult == MessageBoxResult.OK)
                                {
                                    this.Hide();
                                    MainWindow mn = new MainWindow();
                                    mn.Show();
                                    this.Close();
                                }
                            }

                    }
                    else
                    {
                        count++;
                        
                        if (count >= 1 && count != 3)
                        {
                            LoginResult = MessageBox.Show("Wrong Username or Password");


                        }
                        else 
                        {
                            LoginResult = MessageBox.Show("you attempted " + count + " program will now exit");
                            Environment.Exit(0);

                        }

                    }

                }
                catch (Exception ex)
                {
                    Txtbox_username.Text = "";
                    password.Password = "";
                    MessageBox.Show("Error Please Contact administrator : Error Detail : " + ex.Message);
                }
                finally
                {
                    conn.Close();
                    
                }
            }
            else
            {
                MessageBox.Show("Username and Password required");
                
            }

            #endregion
        }


        private void close_login_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Txtbox_password_TextChanged(object sender, TextChangedEventArgs e)
        {
            //_typedInto = !string.IsNullOrEmpty(Txtbox_password.Text);
        }

        private void create_Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            AccountCreationWindow Acw = new AccountCreationWindow();
            Acw.Show();
            this.Close();
        }
    }
}
