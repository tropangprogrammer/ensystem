﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Data.OleDb;
using System.Windows.Threading;
using System.Data;
using EnSystem.Classes;

namespace EnSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        //public string Path = Application.StartupPath + "\\db_Enrollment.accdb";
        OleDbConnection conn = new OleDbConnection(@"Provider=Microsoft.Ace.OLEDB.12.0;Data Source= '" + dbconn.path + "'");
        private static string tuition,miscfee,labfee,increase;
        private static double Total,c_tuition,c_labfee,c_increase,c_miscfee,totalIncrease;
        private static string ispayed = "No",idnumber = loginWindow.userID;
        private bool IsClicked = false;
        DateTime Today = DateTime.Now;

        public MainWindow()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory",Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            InitializeComponent();
            name_Label.Content = "Hello " + loginWindow.fName + " " + loginWindow.lName;
            RefreshGrid();
            comboCourse();
            Combo_course.SelectedIndex = combo_status.SelectedIndex = combo_yearlevel.SelectedIndex = 0;
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(1);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        private void check()
        {
            #region check
            if (combo_status.SelectedIndex == 1 && combo_yearlevel.SelectedIndex > 1)
            {
                MessageBox.Show("You cannot select New Student if year level is not 1st Year ");
                combo_status.SelectedIndex = 0;
                combo_yearlevel.SelectedIndex = 0;
            }
            else if (combo_status.SelectedIndex == 3 && combo_yearlevel.SelectedIndex == 1)
            {
                MessageBox.Show("You cannot select Transfer student if year level is one ");
                combo_status.SelectedIndex = 0;
                combo_yearlevel.SelectedIndex = 0;
            }
            else if (combo_yearlevel.SelectedIndex == 1 && combo_status.SelectedIndex == 2)
            {
                MessageBox.Show("You cannot select Old student if year level is one");
                combo_status.SelectedIndex = 0;
                combo_yearlevel.SelectedIndex = 0;
            }
            #endregion
        }
        private void resetCB()
        {
            Combo_course.SelectedIndex = combo_status.SelectedIndex = combo_yearlevel.SelectedIndex = 0;
        }

        private void comboCourse()
        {
            string  select= "SELECT tbl_Courses.T_Course FROM tbl_Courses; ";
            conn.Open();
            OleDbCommand cmd = new OleDbCommand(select, conn);
            OleDbDataReader DR = cmd.ExecuteReader();
            while (DR.Read())
            {
                Combo_course.Items.Add(DR[0]);
            }
            conn.Close();
        }
        private void RefreshGrid()
        {
            #region Refresh
            conn.Open();
            string select = "SELECT tbl_Enroll.T_Course AS Course, tbl_Enroll.N_TotalFee AS TotalFee, tbl_Enroll.N_DateEnrolled AS DateEnrolled, tbl_Enroll.N_DateFinished AS DateFinished, tbl_Enroll.N_IsPayed AS IsPayed, tbl_Enroll.N_YearLevel AS YearLevel, tbl_Enroll.N_Status AS Status " +
                "FROM tbl_Enroll WHERE i_IdNumber = '"+idnumber+"'";
            var dtEnrolled = new DataTable();
            var adapter = new OleDbDataAdapter(select, conn);
            adapter.Fill(dtEnrolled);
            datagrid_db.ItemsSource = dtEnrolled.AsDataView();
            datagrid_db.Items.Refresh();
            conn.Close();
            #endregion
        }

        private void combo_yearlevel_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            check();
        }
        private void combo_status_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            check();
        }

        private void Rectangle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to quit?", "Closing Application", MessageBoxButton.YesNo, MessageBoxImage.Exclamation) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }

        private void datagrid_db_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        public void b_logout_Click(object sender, RoutedEventArgs e)
        {
            if(MessageBox.Show("Logout?","Logging Out", MessageBoxButton.OKCancel, MessageBoxImage.Question)==MessageBoxResult.OK)
            {                 
                loginWindow gotologin = new loginWindow();
                this.Hide();
                gotologin.Show();
                this.Close();
            }
          
        }
        void timer_Tick(object sender, EventArgs e)
        {
            test.Content = DateTime.Now;
        }
        private void b_info_Click(object sender, RoutedEventArgs e)
        {
            InformationWindow Iw = new InformationWindow();
            Iw.Show();
        }
        #region empty
        private void txtbox_fname_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtbox_lname_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtbox_idnum_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void txtbox_contact_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
        }
        #endregion

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            #region ENROLL BUTTON
            IsClicked = true;
            if (Combo_course.SelectedIndex>0 && combo_status.SelectedIndex>0 && combo_yearlevel.SelectedIndex>0)
            {
                if (MessageBox.Show("You are enrolling "+Combo_course.Text, "Enroll", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    try
                    {
                        conn.Open();                       
                        string selectTuition = "SELECT tbl_Tuition.* FROM tbl_Tuition " +
                            "WHERE T_Status = '"+combo_status.Text+ "' AND T_YearLevel = '" + combo_yearlevel.Text + "' AND T_Course = '" + Combo_course.Text + "';";
                        OleDbCommand cmd = new OleDbCommand(selectTuition);
                        cmd.Connection = conn;
                        OleDbDataReader read = cmd.ExecuteReader();
                        while (read.Read())
                        {
                            miscfee = read["T_MiscFee"].ToString();
                            labfee = read["T_LabFee"].ToString();
                            tuition= read["T_TuitionFee"].ToString();
                            increase = read["T_Increase"].ToString();
                        }
                        conn.Close();
                        c_miscfee = Convert.ToDouble(miscfee);
                        c_labfee = Convert.ToDouble(labfee);
                        c_tuition = Convert.ToDouble(tuition);
                        c_increase = Convert.ToDouble(increase);
                        totalIncrease = c_tuition * c_increase;
                        Total = c_miscfee + c_labfee + c_tuition + totalIncrease;
                        
                        MessageBox.Show("Miscfee: "+miscfee + System.Environment.NewLine+
                            "Labfee: "+labfee + System.Environment.NewLine+
                            "Tuition: " +tuition+ System.Environment.NewLine+
                            "Increase: " +totalIncrease+ System.Environment.NewLine+
                            "Total: "+Total);
                        if (MessageBox.Show("Proceed Enrollment?","Enroll", MessageBoxButton.OKCancel) == MessageBoxResult.OK)
                        {
                            using (OleDbCommand addcmd = new OleDbCommand("INSERT INTO tbl_Enroll(T_Course,N_TotalFee,i_IdNumber,N_DateEnrolled,N_IsPayed,N_YearLevel,N_Status)VALUES(?,?,?,?,?,?,?);", conn))
                            {
                                addcmd.CommandType = CommandType.Text;
                                addcmd.Parameters.AddWithValue("T_Course",Combo_course.Text);
                                addcmd.Parameters.AddWithValue("N_TotalFee",Total);
                                addcmd.Parameters.AddWithValue("i_IdNumber",idnumber);
                                addcmd.Parameters.AddWithValue("N_DateEnrolled",Today.ToLongDateString());
                                addcmd.Parameters.AddWithValue("N_IsPayed", ispayed);
                                addcmd.Parameters.AddWithValue("N_YearLevel",combo_yearlevel.Text);
                                addcmd.Parameters.AddWithValue("N_Status", combo_status.Text);
                                conn.Open();
                                addcmd.ExecuteNonQuery();
                                conn.Close();
                            }
                            using (OleDbCommand updatecmd = new OleDbCommand("UPDATE tbl_Info SET T_Course = @Course WHERE i_IdNumber = '" + idnumber + "';", conn))
                            {
                                updatecmd.CommandType = CommandType.Text;
                                updatecmd.Parameters.AddWithValue("@Course", Combo_course.Text);
                                conn.Open();
                                updatecmd.ExecuteNonQuery();
                                conn.Close();
                                
                            }
                            MessageBox.Show("Successfully Enrolled");
                            RefreshGrid();
                            resetCB();
                            IsClicked = false;
                        }
                        else
                        {
                            MessageBox.Show("Enrollment Cancelled");
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error: " + ex);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show("Select course,status and year level");
            }
            #endregion
        }
    }
}
