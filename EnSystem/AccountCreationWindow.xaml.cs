﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Data.OleDb;
using EnSystem.Classes;
using System.Text.RegularExpressions;

namespace EnSystem
{
    /// <summary>
    /// Interaction logic for AccountCreationWindow.xaml
    /// </summary>
    public partial class AccountCreationWindow : Window
    {
        OleDbConnection conn = new OleDbConnection(@"Provider=Microsoft.Ace.OLEDB.12.0; Data Source= '"+dbconn.path+"'");
        string accountDefault = "No";

        public AccountCreationWindow()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory",Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
            InitializeComponent();
            
        }

        private void ACbar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void exit_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            loginWindow lw = new loginWindow();
            lw.Show();
            this.Close();
        }

        private void Create_Account_Click(object sender, RoutedEventArgs e)
        {
            #region Create   
            if (tb_Contact.Text != "" && tb_Fname.Text != "" && tb_IDnumber.Text != ""
                && tb_Lname.Text != "" && tb_UserName.Text != "" && pass1.Password != "" && pass2.Password != "")
            {
                if (pass1.Password == pass2.Password)
                {
                    OleDbCommand CheckIdcmd = new OleDbCommand("SELECT i_IdNumber FROM tbl_Login where i_IdNumber = @ID", conn);
                    CheckIdcmd.Parameters.AddWithValue("@ID", this.tb_IDnumber.Text);
                    conn.Open();
                    var result = CheckIdcmd.ExecuteScalar();
                    conn.Close();
                    if (result != null)
                    {
                        MessageBox.Show(string.Format("Id Number "+tb_IDnumber.Text+ " already exist "));
                    }
                    else
                    {
                        OleDbCommand CheckUsercmd = new OleDbCommand("SELECT e_usern FROM tbl_Login where e_usern = @username", conn);
                        CheckUsercmd.Parameters.AddWithValue("@username", this.tb_UserName.Text);
                        conn.Open();
                        var UserResult = CheckUsercmd.ExecuteScalar();
                        conn.Close();
                        if (UserResult != null)
                        {
                            MessageBox.Show(string.Format("Username " + tb_UserName.Text+" already exist "));
                        }
                        else
                        {
                            try
                            {
                                OleDbCommand cmd = conn.CreateCommand();
                                conn.Open();
                                cmd.CommandText = "Insert into tbl_Info(i_fname,i_Lname,i_contactNum,i_IdNumber)Values('" + tb_Fname.Text + "','" + tb_Lname.Text + "','" + tb_Contact.Text + "','" + tb_IDnumber.Text + "');";
                                cmd.Connection = conn;
                                cmd.ExecuteNonQuery();
                                conn.Close();

                                cmd.Connection = conn;
                                conn.Open();
                                cmd.CommandText = "Insert into tbl_Login(e_usern,e_passw,i_IdNumber,e_is_admin)Values('" + tb_UserName.Text + "','" + pass2.Password + "','" + tb_IDnumber.Text + "','" + accountDefault + "');";
                                cmd.ExecuteNonQuery();
                                conn.Close();
                                MessageBox.Show("Your account is created " + tb_Fname.Text + " " + tb_Lname.Text);

                            }
                            catch (Exception except)
                            {
                                MessageBox.Show(except.Message, "Error");
                            }
                            finally
                            {
                                tb_Fname.Text = tb_Lname.Text = tb_Contact.Text = tb_IDnumber.Text = tb_UserName.Text = pass1.Password = pass2.Password = "";
                                conn.Close();

                            }
                        }
                        
                    }
                }
                else
                {
                    MessageBox.Show("Password Mismatch");
                }

            }
            else
            {
                MessageBox.Show("Answer the required field");
            }
            #endregion
        }
        #region REGEX
        private void tb_IDnumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = Regex.IsMatch(e.Text, "[^0-9,-]+");
        }

        private void tb_Contact_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = Regex.IsMatch(e.Text, "[^0-9]+");
        }
        #endregion
    }
}
